$(document).ready(initSMSConfig);
var resultcell = null;
function initSMSConfig() {
    $.getJSON("smsconfig", valuesFromServer)
    document.getElementById("submitbutton").addEventListener("click",saveValues,false);
    resultcell = document.getElementById("resultcell");
}
function valuesFromServer(data) {
    for (var key in data) {
        var formElement = document.getElementById(key);
        formElement.setAttribute("value", data[key]);
    }
}
function saveValues(event) {
    resultcell.style.display = "table-cell";
    var smsuser = document.getElementById("smsuser").value;
    var smspass = document.getElementById("smspass").value;
    var smsid = document.getElementById("smsid").value;
    var httpaddr = document.getElementById("httpaddr").value;
    var postData = {
        httpaddr: httpaddr,
        smsuser: smsuser,
        smspass: smspass,
        smsid: smsid
    };
    $.ajax({
        type: "POST",
        url: "smsconfig",
        data: postData,
        success: submitSuccessHandler,
        error: submitErrorHandler
    });
}
function submitSuccessHandler(data, textStatus) {
    $(resultcell).empty();
    resultcell.textContent = "Success";
}
function submitErrorHandler(xhttprequest, status, error) {
    $(resultcell).empty();
    resultcell.textContent = xhttprequest.responseText;
} 