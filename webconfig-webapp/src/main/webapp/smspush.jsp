<%-- 
    Document   : smspush
    Created on : Dec 9, 2008, 9:55:38 AM
    Author     : matt
--%>
<%
request.setAttribute("customscript", "smspush.js");
%>
<%@ include file="head.jsp" %>
<div id="content">
    <p>A web based SMS gateway can be used for push notifications to devices</p>
    <p>To use SMS notification, configure the provider below and then configure
    each device to use the SMS provider</p>
    <p>The SMS notification provider has been built for the <a href="http://www.clickatell.com/">Clickatell</a>
    HTTP API. Other providers can be used if they share a similar syntax</p>
    <div id="settings">
        <table>
            <thead>
            </thead>
            <tr>
                <td><label for="httpaddr">HTTP address</label></td>
                <td><input type="text" id="httpaddr"/><br/>
                    <p>Substitution values
                        <ul>
                            <li>
                                &lt;%USER%&gt;: Username configured below
                            </li>
                            <li>
                                &lt;%PASS%&gt;: Password configured below
                            </li>
                            <li>
                                &lt;%APPID%&gt;: Application ID configured below
                            </li>
                            <li>
                                &lt;%TO%&gt;: Phone number of device to be notified
                            </li>
                            <li>
                                &lt;%UDH%&gt;: SMS User data field in hex
                            </li>
                            <li>
                                &lt;%DATA%&gt;: SMS Data in hex.
                            </li>
                        </ul>
                    </p>
                </td>
            </tr>
            <tr>
                <td><label for="smsuser">Gateway username</label></td>
                <td><input type="text" id="smsuser"/></td>
            </tr>
            <tr>
                <td><label for="smspass">Gateway password</label> <small>Enter new password to change</small></td>
                <td><input type="password" id="smspass"/></td>
            </tr>
            <tr>
                <td><label for="smsid">Gateway application ID</label></td>
                <td><input type="text" id="smsid"/></td>
            </tr>
            <tr>
                <td><button id="submitbutton">Submit</button></td>
                <td id="resultcell"><img src="ajax-loader.gif" alt="loading animation"/>Saving settings..</td>
            </tr>
        </table>
    </div>
</div>
<%@ include file="footer.jsp" %>