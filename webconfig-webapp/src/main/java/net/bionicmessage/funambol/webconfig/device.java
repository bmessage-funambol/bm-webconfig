/*
 *   WebConfig - a web administration interface for the Funambol DS Server.
 *   Copyright (C) 2008 Mathew McBride
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.bionicmessage.funambol.webconfig;

import com.funambol.framework.server.ConvertDatePolicy;
import com.funambol.framework.server.Sync4jDevice;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.bionicmessage.funambol.configuration.DeviceUtils;
import net.bionicmessage.funambol.configuration.MiscUtils;
import net.bionicmessage.funambol.configuration.PrincipalUtils;

/**
 *
 * @author matt
 */
public class device extends HttpServlet {

    

    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        try {
            String searchParam = request.getParameter("searchParam");
            boolean isAdministrator = (session.getAttribute("isadministrator") != null);
            String curuser = (String) session.getAttribute("user");
            Sync4jDevice[] devices = null;
            if (isAdministrator) {
                devices = DeviceUtils.getDevicesBeginningWith(searchParam);
            } else {
                String[] devIdsForUser = PrincipalUtils.allowedDevicesForUser(curuser);
                devices = DeviceUtils.getDevicesByIds(devIdsForUser);
            }
            String[][] devs = new String[devices.length][];
            for (int i = 0; i < devices.length; i++) {
                Sync4jDevice dev = devices[i];
                String[] row = {dev.getDeviceId(),
                    dev.getDescription(),
                    dev.getType(),
                    dev.getTimeZone(),
                    dev.getConvertDatePolicyDescription(),
                    dev.getMsisdn(),
                    dev.getAddress(),
                    dev.getNotificationBuilder(),
                    dev.getNotificationSender()
                };
                devs[i] = MiscUtils.sanitizeStringArray(row);
            }
            Gson gson = new Gson();
            out.println(gson.toJson(devs));
        } catch (Exception ex) {
            Logger.getLogger(device.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            out.close();
        }
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/plain;charset=utf-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        boolean isAdministrator = (session.getAttribute("isadministrator") != null);
        String curuser = (String) session.getAttribute("user");
        // Get the vars
        String device = request.getParameter("device");
        String description = request.getParameter("description");
        String type = request.getParameter("type");
        String timezone = request.getParameter("timezone");
        boolean convertDate = Boolean.parseBoolean(request.getParameter("convert"));
        String msisdn = request.getParameter("msisdn");
        String address = request.getParameter("address");
        String notifybuild = request.getParameter("notifybuild");
        String notifysend = request.getParameter("notifysend");
        Sync4jDevice devToModify;
        try {
            if (!isAdministrator && !PrincipalUtils.isUserAllowedToModifyDevice(device, curuser)) {
                response.setStatus(500);
                out.println("You are not authorized to edit this device");
                out.close();
                return;
            }
            devToModify = DeviceUtils.getDeviceById(device);
            if (devToModify == null) {
                response.setStatus(500);
                out.println("Cannot find device to modify!");
                out.close();
                return;
            }
            devToModify.setDescription(description);
            devToModify.setType(type);
            devToModify.setTimeZone(timezone);
            if (convertDate) {
                devToModify.setConvertDatePolicy(ConvertDatePolicy.CONVERT_DATE);
            } else {
                devToModify.setConvertDatePolicy(ConvertDatePolicy.NO_CONVERT_DATE);
            }
            devToModify.setMsisdn(msisdn);
            devToModify.setAddress(address);
            devToModify.setNotificationBuilder(notifybuild);
            devToModify.setNotificationSender(notifysend);
            DeviceUtils.setDevice(devToModify);
        } catch (Exception ex) {
            response.setStatus(500);
            out.println(ex.getMessage());
            out.close();
            return;
        }
        out.println("Success");
        out.close();
    }

    /** 
     * Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Device information servlet";
    }
}
