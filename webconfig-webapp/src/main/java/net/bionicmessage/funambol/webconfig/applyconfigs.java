package net.bionicmessage.funambol.webconfig;
/*
 *   WebConfig - a web administration interface for the Funambol DS Server.
 *   Copyright (C) 2008-2010 Mathew McBride
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import com.funambol.framework.server.Sync4jSource;
import com.funambol.framework.server.store.PersistentStore;
import com.funambol.framework.tools.beans.BeanException;
import com.funambol.server.config.Configuration;
import com.funambol.server.config.EngineConfiguration;
import com.funambol.server.config.ServerConfiguration;
import com.google.gson.Gson;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Properties;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.bionicmessage.funambol.configuration.SyncSourceUtils;
import net.bionicmessage.funambol.framework.Constants;
import net.bionicmessage.funambol.groupdav.calendar.CalendarSyncSource;
import net.bionicmessage.funambol.groupdav.contacts.ContactSyncSource;
import net.bionicmessage.funambol.httpauth.HTTPAuthenticationOfficer;
import net.bionicmessage.objects.StoreConstants;

/**
 *
 * @author matt
 */
public class applyconfigs extends HttpServlet {

    static final String[] calendarSyncSources = {"groupdav/groupdav/groupdavCal/event.xml",
        "groupdav/groupdav/groupdavCal/groupdav-s60-v.xml",
        "groupdav/groupdav/groupdavCal/task.xml"
     };
    static final String[] calendarSyncNames = {"event",
        "groupdav-s60-v",
        "task"
    };
    static final String[] addrSyncSources = {
        "groupdav/groupdav/groupdavContact/card.xml"
    };
    static final String[] addrSyncNames = {"card"};


    ArrayList<String> eventList = null;
    ArrayList<String> eventResult = null;
    private void addEvent(String name, String result) {
        eventList.add(name);
        eventResult.add(result);
    }
    private void addException(String name, Exception problem) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        pw.print("FAILURE:");
        problem.printStackTrace(pw);
        pw.close();
        eventList.add(name);
        eventResult.add(sw.toString());
    }
    private String outputJson() {
        Gson gson = new Gson();
        String[][] results = new String[eventList.size()][];
        for (int i = 0; i < results.length; i++) {
            String name = eventList.get(i);
            String result = eventResult.get(i);
            String exception = null;
            if (result.startsWith("FAILURE:")) {
                exception = result.replaceFirst("FAILURE:", "");
                String[] row = {name, "Failure",exception};
                results[i] = row;
            } else {
                String[] row = {name, result};
                results[i] = row;
            }
        }
        return gson.toJson(results);
    }
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        try {
            // Keep all output for later when we stick it into a proper JSP

            Configuration conf = Configuration.getConfiguration();
            PersistentStore ps = conf.getStore();
            String srvRoot = request.getParameter("server");
            String srvType = request.getParameter("type");
            boolean useHTTPAuth = ("http".equals(request.getParameter("auth")));
            boolean isCitadel = "citadel".equals(srvType);
            eventList = new ArrayList(25);
            eventResult = new ArrayList(25);
            /*
             * Start the creation thread and redirect to wizard2.jsp
             */

            if (srvRoot.charAt(srvRoot.length() - 1) != '/') {
                srvRoot = srvRoot + "/";
            }
            
            // Figure out the http://host:port/ for the server 
            URL server = new URL(srvRoot);
            StringBuilder actualServer = new StringBuilder(server.getProtocol());
            actualServer.append("://");
            actualServer.append(server.getHost());
            if (server.getPort() != -1) {
                actualServer.append(":");
                actualServer.append(server.getPort());
            }
            actualServer.append("/");
            String srv = actualServer.toString();
            addEvent("GroupDAV Server",srv);
            
            // Now lets try and find the paths for the server
            String propertyPath = String.format("/WEB-INF/%s.properties", srvType);
            InputStream propInputStream = getServletContext().getResourceAsStream(propertyPath);
            Properties serverPathProperties = new Properties();
            serverPathProperties.load(propInputStream);

            serverPathProperties = applyBasePath(serverPathProperties, server);

            String calendarPath = serverPathProperties.getProperty("calendar");
            String taskPath = serverPathProperties.getProperty("task");
            String addressPath = serverPathProperties.getProperty("address");
            
            String rootPath = conf.getFunambolHome();
            addEvent("Server type",srvType);
            addEvent("Authentication",request.getParameter("auth"));
            addEvent("Funambol root",rootPath);
            addEvent("Calendar path",calendarPath);
            addEvent("Task path",taskPath);
            addEvent("Address path",addressPath);
            out.println();
            // First do the calendars
            for (int i = 0; i < calendarSyncSources.length; i++) {
                try {
                    String beanXml = calendarSyncSources[i];
                    String exampleBean = beanXml.replace(".xml", ".example.xml");
                    String name = calendarSyncNames[i];
                    SyncSourceUtils.deleteSyncSourceByURI(name);
                    CalendarSyncSource css =
                            (CalendarSyncSource) conf.getBeanInstanceByName(exampleBean, false);
                    String storePath = rootPath.toString() + File.separator + name+ File.separator;
                    Properties props = css.getConnectorProperties();
                    props.setProperty(Constants.SERVER_HOST, srv);
                    props.setProperty(Constants.STOREDIR_PATH, storePath);
                    if (name.contains("event") ||
                            name.contains("s60")) {
                        props.setProperty(Constants.SOURCE_LOCATION_BASE + "default",
                                calendarPath);
                    } else if (name.contains("task")) {
                        props.setProperty(Constants.SOURCE_LOCATION_BASE + "default",
                                taskPath);
                    }
                    if (name.contains("s60")) {
                        props.setProperty(Constants.SOURCE_LOCATION_BASE + "tasks",
                                taskPath);
                    }
                    if ("opengroupware".equals(srvType)) {
                        // Force bulk download OFF
                        props.setProperty(StoreConstants.PROPERTY_SERVER_MODE, "groupdav");
                    }
                    css.setConnectorProperties(props);
                    conf.setBeanInstance(beanXml, css);
                    Sync4jSource sj = new Sync4jSource(name,
                            beanXml,
                            "groupdavCal",
                            name);
                    ps.store(sj);
                    addEvent(calendarSyncNames[i],"Success");
                } catch (Exception e) {
                   addException(calendarSyncNames[i],e); 
                }
            }
            // Next do the contacts
            for (int i = 0; i < addrSyncSources.length; i++) {
                String source = addrSyncSources[i];
                String name = addrSyncNames[i];
                String exampleBean = source.replace(".xml", ".example.xml");
                String storePath = rootPath.toString() + File.separator + name + File.separator;
                try {
                    SyncSourceUtils.deleteSyncSourceByURI(name);
                    ContactSyncSource css =
                            (ContactSyncSource) conf.getBeanInstanceByName(exampleBean, false);
                    Properties props = css.getConnectorProperties();
                    props.setProperty(Constants.SERVER_HOST, srv);
                    props.setProperty(Constants.SOURCE_LOCATION_BASE + "default",
                            addressPath);
                    props.setProperty(Constants.STOREDIR_PATH, storePath);
                    if ("opengroupware".equals(srvType)) {
                        // Force bulk download OFF
                        props.setProperty(StoreConstants.PROPERTY_SERVER_MODE, "groupdav");
                    }
                    css.setConnectorProperties(props);
                    conf.setBeanInstance(source, css);
                    Sync4jSource sj = new Sync4jSource(name,
                            source,
                            "groupdavContact",
                            name);
                    ps.store(sj);
                    addEvent(name,"Success");
                } catch (Exception e) {
                    addException(name,e);
                }
            }
            if ("http".equals(useHTTPAuth)) {
                try {
                    applyHTTPOfficer(conf, srvRoot);
                    addEvent("HTTP Authentication", "Success");
                } catch (Exception e) {
                    addException("HTTP Authentication", e);
                }
            } else {
                try {
                    applyDBOfficer(conf);
                    addEvent("Database Authentication", "Success");
                } catch (Exception e) {
                    addException("Database Authentication", e);
                }
            }
            
            /* try {
                addSIFTransforms(conf);
                addEvent("Data transformations", "Success");
            } catch (Exception e) {
                addException("Data transofmrations", e);
            } */
            response.setContentType("application/json;charset=utf-8");
            out.print(outputJson());
            out.flush();
        } catch (Exception e) {
            response.setStatus(500);
            response.setContentType("text/plain;charset=utf-8");
            e.printStackTrace(out);
        } finally {
            out.close();
        }
    }

    /** Apply the base path supplied to the GroupDAV locations we 
     * know about. 
     * <br/>
     * <pre>
     * i.e base path: http://egroupware.server/egw/groupdav.php/
     * we know calendar/ is the calendar URL
     * so we set the source as /egw/groupdav.php/calendar/
     * </pre>
     * @param urlProps
     * @param base
     * @return
     * @throws java.net.MalformedURLException
     */
    protected Properties applyBasePath(Properties urlProps, URL base) throws MalformedURLException {
        for (Object key : urlProps.keySet()) {
            String value = (String) urlProps.get(key);
            URL newPath = new URL(base, value);
            String p = newPath.getPath();
            urlProps.setProperty((String) key, p);
        }
        return urlProps;
    }

    /**
     * 
     * @param c
     * @param rootPath
     * @throws com.funambol.framework.tools.beans.BeanException
     */
    protected void applyHTTPOfficer(Configuration c, String rootPath) throws BeanException {
        String httpAuthPath = "net/bionicmessage/funambol/httpauth/HTTPAuthenticationOfficer.xml";
        // Get the example bean
        HTTPAuthenticationOfficer officer =
                (HTTPAuthenticationOfficer) c.getBeanInstanceByName("net/bionicmessage/funambol/httpauth/HTTPAuthenticationOfficer.example.xml");
        officer.setAuthenticationPath(rootPath);
        c.setBeanInstance(httpAuthPath, officer);
        setOfficer(c, httpAuthPath);
    }

    protected void applyDBOfficer(Configuration c) throws BeanException {
        String dbOfficer = "com/funambol/server/security/UserProvisioningOfficer.xml";
        setOfficer(c, dbOfficer);
    }

    protected void setOfficer(Configuration c, String officer) {
        ServerConfiguration sc = c.getServerConfig();
        EngineConfiguration ec = sc.getEngineConfiguration();
        ec.setOfficer(officer);
        sc.setEngineConfiguration(ec);
        c.setServerConfiguration(sc);
    }

    /* protected void addSIFTransforms(Configuration c) throws BeanException {
        String transformPath = "com/funambol/server/engine/transformer/DataTransformerManager.xml";

        DataTransformerManager dtm =
                (DataTransformerManager) c.getBeanInstanceByName(transformPath);
        Map sourceUriTransformations = dtm.getSourceUriTrasformationsRequired();
        for(String source: sifSyncSources) {
            sourceUriTransformations.put(source, "b64");
        }
        c.setBeanInstance(transformPath, dtm);
    } */
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
