/*
 *   WebConfig - a web administration interface for the Funambol DS Server.
 *   Copyright (C) 2008 Mathew McBride
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.bionicmessage.funambol.webconfig;

import com.funambol.framework.security.Sync4jPrincipal;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.bionicmessage.funambol.configuration.PrincipalUtils;

/**
 *
 * @author matt
 */
public class principals extends HttpServlet {

    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        boolean isAdministrator = (session.getAttribute("isadministrator") != null);
        String curUser = (String) session.getAttribute("user");
        try {
            Sync4jPrincipal[] principals = null;
            if (isAdministrator) {
                principals = PrincipalUtils.getAllPrincipals();
            } else {
                principals = PrincipalUtils.principalsForUser(curUser);
            }
            String[][] respdata = new String[principals.length][];
            for (int i = 0; i < principals.length; i++) {
                Sync4jPrincipal sync4jPrincipal = principals[i];
                String[] principalData = {"" + sync4jPrincipal.getId(), sync4jPrincipal.getUsername(),
                    sync4jPrincipal.getDeviceId()
                };
                respdata[i] = principalData;
            }
            Gson gson = new Gson();
            out.println(gson.toJson(respdata));
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatus(500);
            out.println(e.getMessage());
        } finally {
            out.close();
        }
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();

        try {
            response.setStatus(500);
            out.println("Method not implemented");

        } catch (Exception e) {
            e.printStackTrace();
            ;
            out.println(e.getMessage());
        } finally {
            out.close();
        }
    }

    /** 
     * Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
