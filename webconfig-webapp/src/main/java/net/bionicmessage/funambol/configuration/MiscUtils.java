/*
 *   WebConfig - a web administration interface for the Funambol DS Server.
 *   Copyright (C) 2008 Mathew McBride
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.bionicmessage.funambol.configuration;

/**
 * Miscellaneous utilities
 * @author matt
 */
public class MiscUtils {

    /** Change all null references to empty strings */
    public static String[] sanitizeStringArray(String[] in) {
        for (int i = 0; i < in.length; i++) {
            String string = in[i];
            if (string == null) {
                in[i] = "";
            }
        }
        return in;
    }

    /** Change all empty string refs to nulls, 
     * as some classes may check for nulls instead of empty */
    public static String[] desantitizeStringArray(String[] in) {
        for (int i = 0; i < in.length; i++) {
            String string = in[i];
            if (string.length() == 0) {
                in[i] = null;
            
            }
        }
        return in;
    }
}
