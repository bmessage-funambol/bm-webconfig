/*
 *   WebConfig - a web administration interface for the Funambol DS Server.
 *   Copyright (C) 2008 Mathew McBride
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.bionicmessage.funambol.configuration;

import com.funambol.framework.engine.source.SyncSource;
import com.funambol.framework.filter.AllClause;
import com.funambol.framework.filter.WhereClause;
import com.funambol.framework.server.Sync4jSource;
import com.funambol.framework.server.store.PersistentStore;
import com.funambol.framework.server.store.PersistentStoreException;
import com.funambol.server.config.Configuration;

/**
 *
 * @author matt
 */
public class SyncSourceUtils {
    public static Sync4jSource getSourceByURI(String uri) throws PersistentStoreException {
        String[] values = {uri};
        WhereClause wc = new WhereClause("uri",values, WhereClause.OPT_EQ, false);
        Configuration c = Configuration.getConfiguration();
        PersistentStore ps = c.getStore();
        Sync4jSource[] results = (Sync4jSource[]) ps.read(new Sync4jSource(), wc);
        if (results.length == 1)
            return results[0];
        return null;   
    }
    public static void deleteSyncSourceByURI(String uri) throws PersistentStoreException {
        Sync4jSource existingSource = getSourceByURI(uri);
        if (existingSource != null) {
            Configuration c = Configuration.getConfiguration();
            PersistentStore ps = c.getStore();
            ps.delete(existingSource);
        }
    }
    public static String[] getSyncSourceURIs() throws PersistentStoreException {
        AllClause c = new AllClause();
        Configuration conf = Configuration.getConfiguration();
        PersistentStore ps = conf.getStore();
        Sync4jSource[] results = (Sync4jSource[]) ps.read(new Sync4jSource(), c);
        String[] uris = new String[results.length];
        for (int i = 0; i < results.length; i++) {
            Sync4jSource source = results[i];
            uris[i] = source.getUri();
        }
        return uris;
    }
}
